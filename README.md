# autopilot-lab

Place to play around with Joyent's autopilot system.

## deploy to triton:

Requires
 - docker
 - docker-compose
 - [Triton CLI](https://docs.joyent.com/public-cloud/api-access/cloudapi)

Use `triton profiles` to make sure you actually have a profile set.

Run `eval $(triton env)` to connect your docker client to Triton.

Use the docker CLI (`docker-compose up`, `docker-compose down`, ...)

## docker compose says 'CERTIFICATE_VERIFY_FAILED'
`brew install docker-compose; brew link --overwrite docker-compose`
